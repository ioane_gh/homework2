package com.example.homework2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        tryAgain()
    }
    var firstPlayer: Boolean = true

    private fun init(){

        button00.setOnClickListener {
            clickCheck(button00)
        }
        button01.setOnClickListener {
            clickCheck(button01)
        }
        button02.setOnClickListener {
            clickCheck(button02)
        }
        button10.setOnClickListener {
            clickCheck(button10)
        }
        button11.setOnClickListener {
            clickCheck(button11)
        }
        button12.setOnClickListener {
            clickCheck(button12)
        }
        button20.setOnClickListener {
            clickCheck(button20)
        }
        button21.setOnClickListener {
            clickCheck(button21)
        }
        button22.setOnClickListener {
            clickCheck(button22)
        }

    }
    private fun clickCheck(button: Button){
        if (firstPlayer){
            button.text = "X"
            firstPlayer = !firstPlayer
            button.isClickable = false
        }else{
            button.text = "O"
            firstPlayer = !firstPlayer
            button.isClickable = false
        }
        checkWinner()
    }

    private fun checkWinner(){
        if (button00.text.toString() == button01.text.toString() && button01.text.toString() == button02.text.toString() && emptyBox(button00)){
            Toast.makeText(this,"winner is ${button00.text.toString()}", Toast.LENGTH_SHORT).show()
            stopGame()
        }
        if (button10.text.toString() == button11.text.toString() && button11.text.toString() == button12.text.toString() && emptyBox(button11)){
            Toast.makeText(this,"winner is ${button11.text.toString()}", Toast.LENGTH_SHORT).show()
            stopGame()
        }
        if (button20.text.toString() == button21.text.toString() && button21.text.toString() == button22.text.toString() && emptyBox(button20)){
            Toast.makeText(this,"winner is ${button21.text.toString()}", Toast.LENGTH_SHORT).show()
            stopGame()
        }
        if (button00.text.toString() == button10.text.toString() && button10.text.toString() == button20.text.toString() && emptyBox(button10)){
            Toast.makeText(this,"winner is ${button00.text.toString()}", Toast.LENGTH_SHORT).show()
            stopGame()
        }
        if (button01.text.toString() == button11.text.toString() && button11.text.toString() == button21.text.toString() && emptyBox(button11)){
            Toast.makeText(this,"winner is ${button01.text.toString()}", Toast.LENGTH_SHORT).show()
            stopGame()
        }
        if (button02.text.toString() == button12.text.toString() && button12.text.toString() == button22.text.toString() && emptyBox(button12)){
            Toast.makeText(this,"winner is ${button02.text.toString()}", Toast.LENGTH_SHORT).show()
            stopGame()
        }
        if (button00.text.toString() == button11.text.toString() && button11.text.toString() == button22.text.toString() && emptyBox(button11)){
            Toast.makeText(this,"winner is ${button00.text.toString()}", Toast.LENGTH_SHORT).show()
            stopGame()
        }
        if (button02.text.toString() == button11.text.toString() && button11.text.toString() == button20.text.toString() && emptyBox(button11)){
            Toast.makeText(this,"winner is ${button02.text.toString()}", Toast.LENGTH_SHORT).show()
            stopGame()
        }
        if (emptyBox(button00) && emptyBox(button01) && emptyBox(button02) && emptyBox(button10) && emptyBox(button11) && emptyBox(button12) && emptyBox(button20) && emptyBox(button21) && emptyBox(button22) ){
            Toast.makeText( this,"draw", Toast.LENGTH_SHORT).show()
        }
    }
    private fun tryAgain(){
        tryAgain.setOnClickListener {
            button00.text = ""
            button01.text = ""
            button02.text = ""
            button10.text = ""
            button11.text = ""
            button12.text = ""
            button20.text = ""
            button21.text = ""
            button22.text = ""
            button00.isClickable = true
            button01.isClickable = true
            button02.isClickable = true
            button10.isClickable = true
            button11.isClickable = true
            button12.isClickable = true
            button20.isClickable = true
            button21.isClickable = true
            button22.isClickable = true
            firstPlayer = true
        }
    }
    private fun stopGame(){
        button00.isClickable = false
        button01.isClickable = false
        button02.isClickable = false
        button10.isClickable = false
        button11.isClickable = false
        button12.isClickable = false
        button20.isClickable = false
        button21.isClickable = false
        button22.isClickable = false
    }

    private fun emptyBox(button: Button): Boolean {
        return button.text.toString().isNotBlank()
    }
}